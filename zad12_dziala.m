r = 0.002;		% promien drutu (m)
R = 2 * 0.0254;		% promien zwoju (m)
m = 0.01;		% masa (kg)
t = 2;		% stala czasowa tlumienia (s)
g = 9.80665;			% przyspieszenie ziemskie (m/s^2)
G = 80E9;		% modul kirchhoffa (Pa)
N = 2:1:50;		% ilosc zwojow 
f = 0:0.5:500;		% czestotliwosc

Fmax = m * g;       %sila
a = Fmax/m; 		% alfa
b = 1/(2 * t);		% beta


for i = 1:size(N, 2);   %petla od 1 do najwyzszej wartosci N
	k = (G * r^4)/(R^3 * N(i) * 4); %stala sprezystosci
	omega = sqrt(k/m);
	for j = 1:size(f, 2);  %petla od 1 do najwyzszej czestotliwosci
		A(j, i) = a/(sqrt((omega^2 - f(j)^2)^2) + (4 * b^2 *f(j)^2));
	end
	[x(i), y] = max(A(:, i));  %szukanie najwyzszych amplitud po indeksie i
	omega0(i) = f(y)/2/pi;

	subplot(2, 1, 1);  %nanoszenie wszystkich wartosci na wykres A(omega_0)
	plot((f/2/pi), A)
end

xlim([0 80]);  %ograniczenie wartosci na osi x
xlabel('Czestotliwosc (Hz)'); % nazwy osi
ylabel('Amplituda (m)');


subplot(2, 1, 2);
plot(N, x);   % tworzenie wykresu A(N)

xlim([0 55]);
xlabel('N');
ylabel('A_{max} (m)');


saveas(gcf, 'resonance.pdf');

fileId = fopen('resonance_analysis_results.dat', 'w'); %tworzenie pliku .dat

%dodawanie staych wartosci do pliku

fprintf(fileId, 'Promien zwoju: R = %.4f m; \nPromien drutu: r = %.3f m; \nMasa: m = %.2f kg; \nStala czasowa tlumienia: t = %.0f s; \nPrzyspieszenie ziemskie: g = %.3f m/s^2; \nModul Kirchhoffa: G = %d Pa \n\n\n', R, r, m, t, g, G);

%dodawanie wszystkich zmiennych wartosci w petli

for i = 1:size(N, 2);
	fprintf(fileId, 'Liczba zwojow sprezyny: N = %.0f; \nNajwieksza amplituda: A = %.4f m; \nCzestotliwosc omega_0: omega_0 = %.4f Hz \n\n\n', N(i), x(i), omega0(i));
end

fclose(fileId); %zamykanie pliku
